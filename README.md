## Content

0. `wordsearch` - eclipse java project, Word Search Kata is coded within this project
	* I used Eclipse IDE for Java Developers, Version: Photon Release (4.8.0)
	* Java 8 is required for this project
	* `./wordsearch/src/org/wordsearch/WordSearch.java` - implementation
	* `./wordsearch/src/org/wordsearch/Run.java` - main class
0. `apache-ant-1.10.5` - ant binaries for running ant build

---

## How to build the wordsearch project

0. `JAVA_HOME` system variable has to point to Java 8 JDK home directory (required by ant)
	* example for Windows: `set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_201`
	* example for Linux/Mac: `JAVA_HOME=/Library/Java/jdk1.8.0_201`
0. issue ant command in the base directory of git repository
	* on Windows: `.\apache-ant-1.10.5\bin\ant -f wordsearch\build.xml`
	* on Linux/Mac: `./apache-ant-1.10.5/bin/ant -f wordsearch/build.xml`

---

## How to run main class with a sample file

0. please make sure that `java` is available from command line (`JDK8/bin` or `JRE8/bin` needs to be in the system `PATH` variable)
0. build the project by following **How to build the wordsearch project**
0. run `java` program
	* `java -cp ./wordsearch/target/code.compile org.wordsearch.Run ./wordsearch/data/validPuzzle.txt`
	* `java -cp ./wordsearch/target/code.compile org.wordsearch.Run ./wordsearch/data/smallPuzzle.txt`
	* `java -cp ./wordsearch/target/code.compile org.wordsearch.Run ./wordsearch/data/largePuzzle.txt`

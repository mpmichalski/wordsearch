package org.wordsearch;

public class SearchAlgorithm {

	public Result search(String word, char[][] letters) {

		int max = letters.length - 1;
		for (int y = 0; y <= max; y++) {
			for (int x = 0; x <= max; x++) {
				if (word.charAt(0) == letters[y][x]) {
					Result result = buildResultToTheEast(x, y, word, letters);
					if (result.isValid())
						return result;
					result = buildResultToTheSouth(x, y, word, letters);
					if (result.isValid())
						return result;
					result = buildResultToTheNorthEast(x, y, word, letters);
					if (result.isValid())
						return result;
					result = buildResultToTheSouthEast(x, y, word, letters);
					if (result.isValid())
						return result;
					result = buildResultToTheWest(x, y, word, letters);
					if (result.isValid())
						return result;
					result = buildResultToTheNorth(x, y, word, letters);
					if (result.isValid())
						return result;
					result = buildResultToTheSouthWest(x, y, word, letters);
					if (result.isValid())
						return result;
					result = buildResultToTheNorthWest(x, y, word, letters);
					if (result.isValid())
						return result;
				}
			}
		}
		return null;
	}

	private Result buildResultToTheEast(int x, int y, String word, char[][] letters) {

		Result result = new Result();
		result.setWord(word);

		int max = letters.length - 1;
		char[] wordChars = word.toCharArray();
		for (int i = 0; i < wordChars.length && x + i <= max && wordChars[i] == letters[y][x + i]; i++) {
			result.addCoordinates(new Coordinates(x + i, y));
		}

		return result;
	}

	private Result buildResultToTheSouth(int x, int y, String word, char[][] letters) {

		Result result = new Result();
		result.setWord(word);

		int max = letters.length - 1;
		char[] wordChars = word.toCharArray();
		for (int i = 0; i < wordChars.length && y + i <= max && wordChars[i] == letters[y + i][x]; i++) {
			result.addCoordinates(new Coordinates(x, y + i));
		}

		return result;
	}

	private Result buildResultToTheNorthEast(int x, int y, String word, char[][] letters) {

		Result result = new Result();
		result.setWord(word);

		int max = letters.length - 1;
		int min = 0;
		char[] wordChars = word.toCharArray();
		for (int i = 0; i < wordChars.length && x + i <= max && y - i >= min
				&& wordChars[i] == letters[y - i][x + i]; i++) {
			result.addCoordinates(new Coordinates(x + i, y - i));
		}

		return result;
	}

	private Result buildResultToTheSouthEast(int x, int y, String word, char[][] letters) {

		Result result = new Result();
		result.setWord(word);

		int max = letters.length - 1;
		char[] wordChars = word.toCharArray();
		for (int i = 0; i < wordChars.length && x + i <= max && y + i <= max
				&& wordChars[i] == letters[y + i][x + i]; i++) {
			result.addCoordinates(new Coordinates(x + i, y + i));
		}

		return result;
	}

	private Result buildResultToTheWest(int x, int y, String word, char[][] letters) {

		Result result = new Result();
		result.setWord(word);

		int min = 0;
		char[] wordChars = word.toCharArray();
		for (int i = 0; i < wordChars.length && x - i >= min && wordChars[i] == letters[y][x - i]; i++) {
			result.addCoordinates(new Coordinates(x - i, y));
		}

		return result;
	}

	private Result buildResultToTheNorth(int x, int y, String word, char[][] letters) {

		Result result = new Result();
		result.setWord(word);

		int min = 0;
		char[] wordChars = word.toCharArray();
		for (int i = 0; i < wordChars.length && y - i >= min && wordChars[i] == letters[y - i][x]; i++) {
			result.addCoordinates(new Coordinates(x, y - i));
		}

		return result;
	}

	private Result buildResultToTheSouthWest(int x, int y, String word, char[][] letters) {

		Result result = new Result();
		result.setWord(word);

		int max = letters.length - 1;
		int min = 0;
		char[] wordChars = word.toCharArray();
		for (int i = 0; i < wordChars.length && x - i >= min && y + i <= max
				&& wordChars[i] == letters[y + i][x - i]; i++) {
			result.addCoordinates(new Coordinates(x - i, y + i));
		}

		return result;
	}

	private Result buildResultToTheNorthWest(int x, int y, String word, char[][] letters) {

		Result result = new Result();
		result.setWord(word);

		int min = 0;
		char[] wordChars = word.toCharArray();
		for (int i = 0; i < wordChars.length && x - i >= min && y - i >= min
				&& wordChars[i] == letters[y - i][x - i]; i++) {
			result.addCoordinates(new Coordinates(x - i, y - i));
		}

		return result;
	}

}

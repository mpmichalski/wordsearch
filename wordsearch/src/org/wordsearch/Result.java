package org.wordsearch;

import java.util.ArrayList;
import java.util.List;

public class Result {

	private String word = null;
	private List<Coordinates> coordinatesList = new ArrayList<>();

	public void setWord(String word) {
		this.word = word;
	}

	public String getWord() {
		return this.word;
	}

	public void addCoordinates(Coordinates c) {
		if (c != null) {
			coordinatesList.add(c);
		}
	}

	public int getCoordinatesListSize() {
		return coordinatesList.size();
	}

	public boolean isValid() {

		return word != null && word.length() >= 2 && coordinatesList.size() == word.length();
	}

	public String stringValue() {

		if (isValid()) {
			StringBuilder builder = new StringBuilder(word);
			builder.append(": ");
			builder.append(coordinatesList.stream().map(Coordinates::stringValue).reduce((s, t) -> s + "," + t).get());
			return builder.toString();
		}
		return null;
	}

}

package org.wordsearch;

import java.util.List;

public class Data {

	private final List<String> words;
	private final char[][] letters;

	public Data(List<String> words, char[][] letters) {

		this.words = words;
		this.letters = letters;
	}

	public List<String> getWords() {
		return words;
	}

	public char[][] getLetters() {
		return letters;
	}

}

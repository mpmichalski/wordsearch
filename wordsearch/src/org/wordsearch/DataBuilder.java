package org.wordsearch;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataBuilder {

	public Data buildData(Path path) throws IOException {

		List<String> lines = Files.readAllLines(path);
		List<String> words = new ArrayList<>();
		Collections.addAll(words, lines.get(0).split(","));

		char[][] letters = null;
		for (int index = 1; index < lines.size(); index++) {

			char[] charArray = lines.get(index).replaceAll(",", "").toCharArray();
			if (letters == null) {
				letters = new char[charArray.length][];
			}
			letters[index - 1] = charArray;
		}

		return new Data(words, letters);
	}

}

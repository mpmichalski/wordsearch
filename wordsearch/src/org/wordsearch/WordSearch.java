package org.wordsearch;

import java.io.IOException;
import java.nio.file.Paths;

public class WordSearch {

	public void searchAndPrint(String puzzleFileName) {

		Data data = null;
		try {
			data = new DataBuilder().buildData(Paths.get(puzzleFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (data != null) {
			SearchAlgorithm sa = new SearchAlgorithm();
			char[][] letters = data.getLetters();

			data.getWords().stream().map(s -> {
				return sa.search(s, letters);
			}).filter(r -> r != null).forEach(a -> {
				System.out.println(a.stringValue());
			});
		}

	}

}

package org.wordsearch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

public class DataBuilderTest {

	DataBuilder builder = null;

	@Before
	public void setUp() {
		builder = new DataBuilder();
	}

	@Test(expected = IOException.class)
	public void whenBuildDataIsCalledWithInvalidPathThenItThrowsIOException() throws IOException {

		builder = new DataBuilder();
		builder.buildData(Paths.get("data/invalidPath.txt"));
	}

	@Test
	public void whenBuildDataIsCalledWithValidPathThenItReturnsDataObject() throws IOException {
		Data data = builder.buildData(Paths.get("data/validPuzzle.txt"));
		assertNotNull(data);
	}

	@Test
	public void whenBuildDataIsCalledWithValidPathThenItReturnsDataObjectWithValidWords() throws IOException {
		Data data = builder.buildData(Paths.get("data/validPuzzle.txt"));
		String words = data.getWords().stream().reduce((s, t) -> s + "," + t).get();
		assertEquals("BONES,KHAN,KIRK,SCOTTY,SPOCK,SULU,UHURA", words);
	}

	@Test
	public void whenBuildDataIsCalledWithValidPathThenItReturnsDataObjectWithValidLetters() throws IOException {
		Data data = builder.buildData(Paths.get("data/validPuzzle.txt"));
		assertEquals("U,M,K,H,U,L,K,I,N,V,J,O,C,W,E",
				new String(data.getLetters()[0]).replace("", ",").substring(1, 30));
		assertEquals("L,L,S,H,K,Z,Z,W,Z,C,G,J,U,Y,G",
				new String(data.getLetters()[1]).replace("", ",").substring(1, 30));
		assertEquals("H,S,U,P,J,P,R,J,D,H,S,B,X,T,G",
				new String(data.getLetters()[2]).replace("", ",").substring(1, 30));
		assertEquals("B,R,J,S,O,E,Q,E,T,I,K,K,G,L,E",
				new String(data.getLetters()[3]).replace("", ",").substring(1, 30));
		assertEquals("A,Y,O,A,G,C,I,R,D,Q,H,R,T,C,D",
				new String(data.getLetters()[4]).replace("", ",").substring(1, 30));
		assertEquals("S,C,O,T,T,Y,K,Z,R,E,P,P,X,P,F",
				new String(data.getLetters()[5]).replace("", ",").substring(1, 30));
		assertEquals("B,L,Q,S,L,N,E,E,E,V,U,L,F,M,Z",
				new String(data.getLetters()[6]).replace("", ",").substring(1, 30));
		assertEquals("O,K,R,I,K,A,M,M,R,M,F,B,A,P,P",
				new String(data.getLetters()[7]).replace("", ",").substring(1, 30));
		assertEquals("N,U,I,I,Y,H,Q,M,E,M,Q,R,Y,F,S",
				new String(data.getLetters()[8]).replace("", ",").substring(1, 30));
		assertEquals("E,Y,Z,Y,G,K,Q,J,P,C,Q,W,Y,A,K",
				new String(data.getLetters()[9]).replace("", ",").substring(1, 30));
		assertEquals("S,J,F,Z,M,Q,I,B,D,B,E,M,K,W,D",
				new String(data.getLetters()[10]).replace("", ",").substring(1, 30));
		assertEquals("T,G,L,B,H,C,B,E,C,H,T,O,Y,I,K",
				new String(data.getLetters()[11]).replace("", ",").substring(1, 30));
		assertEquals("O,J,Y,E,U,L,N,C,C,L,Y,B,Z,U,H",
				new String(data.getLetters()[12]).replace("", ",").substring(1, 30));
		assertEquals("W,Z,M,I,S,U,K,U,R,B,I,D,U,X,S",
				new String(data.getLetters()[13]).replace("", ",").substring(1, 30));
		assertEquals("K,Y,L,B,Q,Q,P,M,D,F,C,K,E,A,B",
				new String(data.getLetters()[14]).replace("", ",").substring(1, 30));
	}

}

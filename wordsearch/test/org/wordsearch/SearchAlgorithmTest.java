package org.wordsearch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SearchAlgorithmTest {

	private SearchAlgorithm searchAlgorithm = null;
	static private char[][] letters = null;
	static private char[][] lettersToEast = null;
	static private char[][] lettersToSouth = null;
	static private char[][] lettersToNorthEast = null;
	static private char[][] lettersToSouthEast = null;
	static private char[][] lettersToWest = null;
	static private char[][] lettersToNorth = null;
	static private char[][] lettersToSouthWest = null;
	static private char[][] lettersToNorthWest = null;

	@BeforeClass
	public static void classSetup() {
		letters = new char[15][];
		letters[0] = "U,M,K,H,U,L,K,I,N,V,J,O,C,W,E".replaceAll(",", "").toCharArray();
		letters[1] = "L,L,S,H,K,Z,Z,W,Z,C,G,J,U,Y,G".replaceAll(",", "").toCharArray();
		letters[2] = "H,S,U,P,J,P,R,J,D,H,S,B,X,T,G".replaceAll(",", "").toCharArray();
		letters[3] = "B,R,J,S,O,E,Q,E,T,I,K,K,G,L,E".replaceAll(",", "").toCharArray();
		letters[4] = "A,Y,O,A,G,C,I,R,D,Q,H,R,T,C,D".replaceAll(",", "").toCharArray();
		letters[5] = "S,C,O,T,T,Y,K,Z,R,E,P,P,X,P,F".replaceAll(",", "").toCharArray();
		letters[6] = "B,L,Q,S,L,N,E,E,E,V,U,L,F,M,Z".replaceAll(",", "").toCharArray();
		letters[7] = "O,K,R,I,K,A,M,M,R,M,F,B,A,P,P".replaceAll(",", "").toCharArray();
		letters[8] = "N,U,I,I,Y,H,Q,M,E,M,Q,R,Y,F,S".replaceAll(",", "").toCharArray();
		letters[9] = "E,Y,Z,Y,G,K,Q,J,P,C,Q,W,Y,A,K".replaceAll(",", "").toCharArray();
		letters[10] = "S,J,F,Z,M,Q,I,B,D,B,E,M,K,W,D".replaceAll(",", "").toCharArray();
		letters[11] = "T,G,L,B,H,C,B,E,C,H,T,O,Y,I,K".replaceAll(",", "").toCharArray();
		letters[12] = "O,J,Y,E,U,L,N,C,C,L,Y,B,Z,U,H".replaceAll(",", "").toCharArray();
		letters[13] = "W,Z,M,I,S,U,K,U,R,B,I,D,U,X,S".replaceAll(",", "").toCharArray();
		letters[14] = "K,Y,L,B,Q,Q,P,M,D,F,C,K,E,A,B".replaceAll(",", "").toCharArray();

		lettersToEast = new char[5][];
		lettersToEast[0] = "O,A,B,O,O".replaceAll(",", "").toCharArray();
		lettersToEast[1] = "O,O,O,E,F".replaceAll(",", "").toCharArray();
		lettersToEast[2] = "O,A,B,C,O".replaceAll(",", "").toCharArray();
		lettersToEast[3] = "O,O,E,F,G".replaceAll(",", "").toCharArray();
		lettersToEast[4] = "O,O,O,O,O".replaceAll(",", "").toCharArray();

		lettersToSouth = new char[5][];
		lettersToSouth[0] = "O,O,O,O,O".replaceAll(",", "").toCharArray();
		lettersToSouth[1] = "A,O,A,O,O".replaceAll(",", "").toCharArray();
		lettersToSouth[2] = "B,O,B,O,O".replaceAll(",", "").toCharArray();
		lettersToSouth[3] = "O,E,C,O,O".replaceAll(",", "").toCharArray();
		lettersToSouth[4] = "O,F,O,O,O".replaceAll(",", "").toCharArray();

		lettersToNorthEast = new char[5][];
		lettersToNorthEast[0] = "O,O,O,O,F".replaceAll(",", "").toCharArray();
		lettersToNorthEast[1] = "O,B,C,E,G".replaceAll(",", "").toCharArray();
		lettersToNorthEast[2] = "A,B,O,F,O".replaceAll(",", "").toCharArray();
		lettersToNorthEast[3] = "A,O,E,O,O".replaceAll(",", "").toCharArray();
		lettersToNorthEast[4] = "O,O,O,O,O".replaceAll(",", "").toCharArray();

		lettersToSouthEast = new char[5][];
		lettersToSouthEast[0] = "A,A,O,E,O".replaceAll(",", "").toCharArray();
		lettersToSouthEast[1] = "O,B,B,O,F".replaceAll(",", "").toCharArray();
		lettersToSouthEast[2] = "O,E,O,C,O".replaceAll(",", "").toCharArray();
		lettersToSouthEast[3] = "O,O,F,O,O".replaceAll(",", "").toCharArray();
		lettersToSouthEast[4] = "O,O,O,G,O".replaceAll(",", "").toCharArray();

		lettersToWest = new char[5][];
		lettersToWest[0] = "O,O,B,A,O".replaceAll(",", "").toCharArray();
		lettersToWest[1] = "F,E,O,O,O".replaceAll(",", "").toCharArray();
		lettersToWest[2] = "O,C,B,A,O".replaceAll(",", "").toCharArray();
		lettersToWest[3] = "O,O,O,O,O".replaceAll(",", "").toCharArray();
		lettersToWest[4] = "O,G,F,E,O".replaceAll(",", "").toCharArray();

		lettersToNorth = new char[5][];
		lettersToNorth[0] = "O,O,F,C,G".replaceAll(",", "").toCharArray();
		lettersToNorth[1] = "O,B,E,B,F".replaceAll(",", "").toCharArray();
		lettersToNorth[2] = "O,A,O,A,E".replaceAll(",", "").toCharArray();
		lettersToNorth[3] = "O,O,O,O,O".replaceAll(",", "").toCharArray();
		lettersToNorth[4] = "O,O,O,O,O".replaceAll(",", "").toCharArray();

		lettersToSouthWest = new char[5][];
		lettersToSouthWest[0] = "O,E,A,A,O".replaceAll(",", "").toCharArray();
		lettersToSouthWest[1] = "F,B,B,O,O".replaceAll(",", "").toCharArray();
		lettersToSouthWest[2] = "O,C,E,O,O".replaceAll(",", "").toCharArray();
		lettersToSouthWest[3] = "O,F,O,O,O".replaceAll(",", "").toCharArray();
		lettersToSouthWest[4] = "G,O,O,O,O".replaceAll(",", "").toCharArray();

		lettersToNorthWest = new char[5][];
		lettersToNorthWest[0] = "O,O,F,O,O".replaceAll(",", "").toCharArray();
		lettersToNorthWest[1] = "G,B,C,E,O".replaceAll(",", "").toCharArray();
		lettersToNorthWest[2] = "O,F,A,B,O".replaceAll(",", "").toCharArray();
		lettersToNorthWest[3] = "O,O,E,O,A".replaceAll(",", "").toCharArray();
		lettersToNorthWest[4] = "O,O,O,O,O".replaceAll(",", "").toCharArray();
	}

	@Before
	public void setup() {
		searchAlgorithm = new SearchAlgorithm();
	}

	@Test
	public void whenSearchIsCalledAndTheWordIsInTheGridToTheEastThenItReturnsValidResultForTheWord() {
		Result result = searchAlgorithm.search("SHKZZ", letters);
		assertEquals("SHKZZ: (2,1),(3,1),(4,1),(5,1),(6,1)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledAndTheWord2IsInTheGridToTheEastThenItReturnsValidResultForTheWord2() {
		Result result = searchAlgorithm.search("MKHULKINVJ", letters);
		assertEquals("MKHULKINVJ: (1,0),(2,0),(3,0),(4,0),(5,0),(6,0),(7,0),(8,0),(9,0),(10,0)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordToTheEastAndItWillFindItToTheEastFartherIfExists() {
		Result result = searchAlgorithm.search("ABC", lettersToEast);
		assertEquals("ABC: (1,2),(2,2),(3,2)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordAtEdgeToTheEastAndItWillFindItToTheEastFartherIfExists() {
		Result result = searchAlgorithm.search("EFG", lettersToEast);
		assertEquals("EFG: (2,3),(3,3),(4,3)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillReturnNullIfTheWordDoesNotExist() {
		Result result = searchAlgorithm.search("ABCDEFG", letters);
		assertNull(result);
	}

	@Test
	public void whenSearchIsCalledAndTheWordIsInTheGridToTheSouthThenItReturnsValidResultForTheWord() {
		Result result = searchAlgorithm.search("ULHBA", letters);
		assertEquals("ULHBA: (0,0),(0,1),(0,2),(0,3),(0,4)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledAndTheWord2IsInTheGridToTheSouthThenItReturnsValidResultForTheWord2() {
		Result result = searchAlgorithm.search("KSUJOO", letters);
		assertEquals("KSUJOO: (2,0),(2,1),(2,2),(2,3),(2,4),(2,5)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordToTheSouthAndItWillFindItToTheSouthFartherIfExists() {
		Result result = searchAlgorithm.search("ABC", lettersToSouth);
		assertEquals("ABC: (2,1),(2,2),(2,3)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordAtEdgeToTheSouth() {
		Result result = searchAlgorithm.search("EFG", lettersToSouth);
		assertNull(result);
	}

	@Test
	public void whenSearchIsCalledAndTheWordIsInTheGridToTheNorthEastThenItReturnsValidResultForTheWord() {
		Result result = searchAlgorithm.search("LM", letters);
		assertEquals("LM: (0,1),(1,0)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledAndTheWord2IsInTheGridToTheNorthEastThenItReturnsValidResultForTheWord2() {
		Result result = searchAlgorithm.search("HLK", letters);
		assertEquals("HLK: (0,2),(1,1),(2,0)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordToTheNorthEastAndItWillFindItToTheNorthEastFartherIfExists() {
		Result result = searchAlgorithm.search("ABC", lettersToNorthEast);
		assertEquals("ABC: (0,3),(1,2),(2,1)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordAtEdgeToTheNorthEastAndItWillFindItToTheNorthEastFartherIfExists() {
		Result result = searchAlgorithm.search("EFG", lettersToNorthEast);
		assertEquals("EFG: (2,3),(3,2),(4,1)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledAndTheWordIsInTheGridToTheSouthEastThenItReturnsValidResultForTheWord() {
		Result result = searchAlgorithm.search("UL", letters);
		assertEquals("UL: (0,0),(0,1)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledAndTheWord2IsInTheGridToTheSouthEastThenItReturnsValidResultForTheWord2() {
		Result result = searchAlgorithm.search("MSP", letters);
		assertEquals("MSP: (1,0),(2,1),(3,2)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordToTheSouthEastAndItWillFindItToTheSouthEastFartherIfExists() {
		Result result = searchAlgorithm.search("ABC", lettersToSouthEast);
		assertEquals("ABC: (1,0),(2,1),(3,2)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordAtEdgeToTheSouthEastAndItWillFindItToTheSouthEastFartherIfExists() {
		Result result = searchAlgorithm.search("EFG", lettersToSouthEast);
		assertEquals("EFG: (1,2),(2,3),(3,4)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledAndTheWordIsInTheGridToTheWestThenItReturnsValidResultForTheWord() {
		Result result = searchAlgorithm.search("MU", letters);
		assertEquals("MU: (1,0),(0,0)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledAndTheWord2IsInTheGridToTheWestThenItReturnsValidResultForTheWord2() {
		Result result = searchAlgorithm.search("LU", letters);
		assertEquals("LU: (5,0),(4,0)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordToTheWestAndItWillFindItToTheWestFartherIfExists() {
		Result result = searchAlgorithm.search("ABC", lettersToWest);
		assertEquals("ABC: (3,2),(2,2),(1,2)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordAtEdgeToTheWestAndItWillFindItToTheWestFartherIfExists() {
		Result result = searchAlgorithm.search("EFG", lettersToWest);
		assertEquals("EFG: (3,4),(2,4),(1,4)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledAndTheWordIsInTheGridToTheNorthThenItReturnsValidResultForTheWord() {
		Result result = searchAlgorithm.search("HLU", letters);
		assertEquals("HLU: (0,2),(0,1),(0,0)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledAndTheWord2IsInTheGridToTheNorthThenItReturnsValidResultForTheWord2() {
		Result result = searchAlgorithm.search("SLM", letters);
		assertEquals("SLM: (1,2),(1,1),(1,0)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordToTheNorthAndItWillFindItToTheNorthFartherIfExists() {
		Result result = searchAlgorithm.search("ABC", lettersToNorth);
		assertEquals("ABC: (3,2),(3,1),(3,0)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordAtEdgeToTheNorthAndItWillFindItToTheNorthFartherIfExists() {
		Result result = searchAlgorithm.search("EFG", lettersToNorth);
		assertEquals("EFG: (4,2),(4,1),(4,0)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledAndTheWordIsInTheGridToTheSouthWestThenItReturnsValidResultForTheWord() {
		Result result = searchAlgorithm.search("KLH", letters);
		assertEquals("KLH: (2,0),(1,1),(0,2)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledAndTheWord2IsInTheGridToTheSouthWestThenItReturnsValidResultForTheWord2() {
		Result result = searchAlgorithm.search("HSSB", letters);
		assertEquals("HSSB: (3,0),(2,1),(1,2),(0,3)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordToTheSouthWestAndItWillFindItToTheSouthWestFartherIfExists() {
		Result result = searchAlgorithm.search("ABC", lettersToSouthWest);
		assertEquals("ABC: (3,0),(2,1),(1,2)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordAtEdgeToTheSouthWestAndItWillFindItToTheSouthWestFartherIfExists() {
		Result result = searchAlgorithm.search("EFG", lettersToSouthWest);
		assertEquals("EFG: (2,2),(1,3),(0,4)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledAndTheWordIsInTheGridToTheNorthWestThenItReturnsValidResultForTheWord() {
		Result result = searchAlgorithm.search("QTJ", letters);
		assertEquals("QTJ: (9,4),(8,3),(7,2)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledAndTheWord2IsInTheGridToTheNorthWestThenItReturnsValidResultForTheWord2() {
		Result result = searchAlgorithm.search("PSM", letters);
		assertEquals("PSM: (3,2),(2,1),(1,0)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordToTheNorthWestAndItWillFindItToTheNorthWestFartherIfExists() {
		Result result = searchAlgorithm.search("ABC", lettersToNorthWest);
		assertEquals("ABC: (4,3),(3,2),(2,1)", result.stringValue());
	}

	@Test
	public void whenSearchIsCalledThenItWillIgnoreIncompleteWordAtEdgeToTheNorthWestAndItWillFindItToTheNorthWestFartherIfExists() {
		Result result = searchAlgorithm.search("EFG", lettersToNorthWest);
		assertEquals("EFG: (2,3),(1,2),(0,1)", result.stringValue());
	}
}

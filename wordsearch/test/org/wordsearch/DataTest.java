package org.wordsearch;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class DataTest {

	@Test
	public void whenDataConstructorIsCalledWithArgumentsThenGettersReturnReferencesPassedInConstructor() {

		List<String> list = new ArrayList<>();
		char[][] array = new char[1][1];

		Data data = new Data(list, array);
		assertTrue(list == data.getWords());
		assertTrue(array == data.getLetters());
	}

}

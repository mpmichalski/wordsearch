package org.wordsearch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

import org.junit.Before;
import org.junit.Test;

public class WordSearchTest {

	private WordSearch wordSearch = null;

	@Before
	public void setup() {
		wordSearch = new WordSearch();
	}

	@Test
	public void whenSearchAndPrintIsCalledWithInvalidFileNameThenItPrintsExceptionToTheStandardErrorAndItPrintsNothingToTheStandardOutput()
			throws UnsupportedEncodingException {

		wordSearch = new WordSearch();

		SystemStreamsHelper ssh = new SystemStreamsHelper();
		ssh.intercept();

		wordSearch.searchAndPrint("data/invalidPuzzle.txt");
		String[] acctual = ssh.capture();

		assertEquals("", acctual[0]);
		assertTrue(acctual[1].contains("Exception:"));

		ssh.reinstate();
	}

	@Test
	public void whenSearchAndPrintIsCalledWithValidFileName1ThenItPrintsResults() throws UnsupportedEncodingException {

		wordSearch = new WordSearch();

		SystemStreamsHelper ssh = new SystemStreamsHelper();
		ssh.intercept();

		wordSearch.searchAndPrint("data/validPuzzle.txt");

		StringBuilder expected = new StringBuilder();
		expected.append("BONES: (0,6),(0,7),(0,8),(0,9),(0,10)").append(System.lineSeparator());
		expected.append("KHAN: (5,9),(5,8),(5,7),(5,6)").append(System.lineSeparator());
		expected.append("KIRK: (4,7),(3,7),(2,7),(1,7)").append(System.lineSeparator());
		expected.append("SCOTTY: (0,5),(1,5),(2,5),(3,5),(4,5),(5,5)").append(System.lineSeparator());
		expected.append("SPOCK: (2,1),(3,2),(4,3),(5,4),(6,5)").append(System.lineSeparator());
		expected.append("SULU: (3,3),(2,2),(1,1),(0,0)").append(System.lineSeparator());
		expected.append("UHURA: (4,0),(3,1),(2,2),(1,3),(0,4)").append(System.lineSeparator());

		String[] acctual = ssh.capture();

		assertEquals(expected.toString(), acctual[0]);
		assertEquals("", acctual[1]);

		ssh.reinstate();
	}

	@Test
	public void whenSearchAndPrintIsCalledWithValidFileName2ThenItPrintsResults() throws UnsupportedEncodingException {

		wordSearch = new WordSearch();

		SystemStreamsHelper ssh = new SystemStreamsHelper();
		ssh.intercept();

		wordSearch.searchAndPrint("data/smallPuzzle.txt");
		StringBuilder expected = new StringBuilder();
		expected.append("AB: (0,0),(1,0)").append(System.lineSeparator());
		expected.append("DC: (1,1),(0,1)").append(System.lineSeparator());
		expected.append("AC: (0,0),(0,1)").append(System.lineSeparator());
		expected.append("DB: (1,1),(1,0)").append(System.lineSeparator());
		expected.append("AD: (0,0),(1,1)").append(System.lineSeparator());
		expected.append("DA: (1,1),(0,0)").append(System.lineSeparator());
		expected.append("BC: (1,0),(0,1)").append(System.lineSeparator());
		expected.append("CB: (0,1),(1,0)").append(System.lineSeparator());

		String[] acctual = ssh.capture();

		assertEquals(expected.toString(), acctual[0]);
		assertEquals("", acctual[1]);

		ssh.reinstate();
	}

	private static class SystemStreamsHelper {

		ByteArrayOutputStream byteArrayOutputStream;
		PrintStream originalOutputStream;
		ByteArrayOutputStream byteArrayErrorStream;
		PrintStream originalErrorStream;

		void intercept() {
			byteArrayOutputStream = new ByteArrayOutputStream();
			PrintStream newOutputStream = new PrintStream(byteArrayOutputStream);
			originalOutputStream = System.out;
			System.setOut(newOutputStream);

			byteArrayErrorStream = new ByteArrayOutputStream();
			PrintStream newErrorStream = new PrintStream(byteArrayErrorStream);
			originalErrorStream = System.err;
			System.setErr(newErrorStream);
		}

		String[] capture() throws UnsupportedEncodingException {
			System.out.flush();
			System.err.flush();
			return new String[] { byteArrayOutputStream.toString("UTF-8"), byteArrayErrorStream.toString("UTF-8") };
		}

		void reinstate() {
			System.setOut(originalOutputStream);
			System.setErr(originalErrorStream);
		}
	}
}

package org.wordsearch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.hamcrest.core.IsAnything;
import org.junit.Before;
import org.junit.Test;

public class ResultTest {

	Result result = null;

	@Before
	public void setup() {
		result = new Result();
	}

	@Test
	public void whenSetWordWasCalledWithAValueThenGetWordReturnsThatValue() {

		result.setWord("ALA");
		assertEquals("ALA", result.getWord());
	}

	@Test
	public void whenResultIsCreatedThenCoordinatesListIsEmptyAndWordIsNull() {

		assertEquals(0, result.getCoordinatesListSize());
		assertNull(result.getWord());
	}

	@Test
	public void whenNotNullCoordinatesIsAddedThenCoordinatesSizeIncreasesByOne() {

		int initialSize = result.getCoordinatesListSize();
		result.addCoordinates(new Coordinates(1, 1));
		assertEquals(initialSize + 1, result.getCoordinatesListSize());
	}

	@Test
	public void whenNullCoordinatesIsAddedThenItIsIgnored() {

		int initialSize = result.getCoordinatesListSize();
		result.addCoordinates(null);
		assertEquals(initialSize, result.getCoordinatesListSize());
	}

	@Test
	public void whenWordIsTwoOrLongerSizeAndNumberOfCoordinatesIsSameAsNumberOfLettersThenIsValidReturnsTrueOtherwiseFalse() {

		result.setWord("AB");
		result.addCoordinates(new Coordinates(1, 1));
		result.addCoordinates(new Coordinates(2, 2));
		assertTrue(result.isValid());

		result.setWord("ABC");
		assertFalse(result.isValid());

		result.setWord(null);
		assertFalse(result.isValid());
	}

	@Test
	public void whenResultIsValidThenStringValueReturnsFormattedRepresentation() {

		result.setWord("AB");
		result.addCoordinates(new Coordinates(1, 1));
		result.addCoordinates(new Coordinates(2, 2));
		assertEquals("AB: (1,1),(2,2)", result.stringValue());
	}

	@Test
	public void whenResultIsNotValidThenStringValueReturnsUndefinedValue() {

		assertThat(result.stringValue(), IsAnything.anything());
	}

}

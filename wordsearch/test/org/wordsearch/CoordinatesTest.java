package org.wordsearch;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CoordinatesTest {

	@Test
	public void whenCoordinatesIsCreatedThenGettersReturnCorrectValue() {

		Coordinates c = new Coordinates(20, 13);
		assertEquals(20, c.getX());
		assertEquals(13, c.getY());
	}

	@Test
	public void whenStringValueIsCalledThenIsReturnInParenthesisRepresentation() {

		Coordinates c = new Coordinates(20, 13);
		assertEquals("(20,13)", c.stringValue());
	}

}
